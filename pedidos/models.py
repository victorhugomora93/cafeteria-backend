from djongo import models

class Pedido(models.Model):
    id = models.IntegerField(primary_key=True, serialize=True)
    mesa=models.IntegerField()
    total=models.IntegerField()
    estado=models.CharField(max_length=20)
    fecha_hora = models.DateField( null=True)
    lista_productos = models.IntegerField(default=0) 
    
    #lista_productos = models.JSONField() fecha_hora = models.DateTimeField(null=True) id = models.IntegerField(primary_key=True)

    def __str__(self) -> str:
        return self.estado